# cake-app

To Build: 
- npm run build

To Run: 
- npm start

Note: Is deployed live via continuous deployment from master via netify at https://competent-fermat-bf48ee.netlify.com/

However, the backend api is http rather than https, meaning blocked due to mixed content. But hey, atleast my logic for connections being unavailable is functioning!! (try submitting a new cake too!)

# Spec

S1 - As a cake lover, I can view all cakes that have been submitted so I can drool at their awe- some tastiness.
Acceptance Criteria
- A simple list showing the image and name of the cake and nothing else
- Done - PR - https://bitbucket.org/downeh/cake-app/pull-requests/1

S2 - As a cake lover, I can submit cakes that I like so everyone can drool at my tasty suggestions.
Acceptance Criteria
- I should be able add a cake from the list of all cakes view
- I should be taken to a view where I am able to specify the name a comment and a yum factor between 1 and 5.
- I should be returned back to the list of cakes after submitting.
- Done - PR - https://bitbucket.org/downeh/cake-app/pull-requests/2

S3 - As a cake lover I can view details about a single cake so that I can see the comment made against it.
Acceptance Criteria
- I can select/click or tap any cake in the list and be taken to a view where I can see the comment/review made.
- I should be able to close or navigate back to the list of cakes once I’ve read the cake details.
- Done - PR - https://bitbucket.org/downeh/cake-app/pull-requests/3

# Technical

Design
- React 16.5
- Bulma - Using a react build for JSX integration

Build
- NPM
- Webpack
- Babel

Misc. npm packages
- valid-url - For parsing http links as valid. regex etc is a little code-ugly. Convenience. 

# Summary

- 3 main levels to this responsive SPA. 
    - Layout
        - Header (functionless)
        - Footer (functionless)
    - AddCakes
        - addCakes - This component allows for adding via POST via fetch and a form
    - ViewCakes
        - viewCakes - This component performs the GET for list of cake objects. Returned as an Array and passed down as props
            - cakeCard - This component is a child of viewCakes, and each cake is mapped to a Box element
            - cakeModal - Thid component is passed the ID of a SINGLE cake object and displays a modal  


# Remarks

- Some edge cases I addressed/observed were:
    - Form validation: ensuring all fields are completed pre-post
    - Form validation: web url is a valid syntax. I DO NOT validate if the link is live, merely format via valid-url package
    - Data sanity: The endpoint data included urls to images which do not resolve. I felt beyond scope of the task however does result in a console party of errors
    - Responsive - fully responsive for mobile/desktop inc. images. 

- Extending:
    - I noticed the api guide included DELETE endpoints via id. This can be easily extended into app either in main list (as ID is passed) or in the view modal as again ID is available. 
    - Adding new cake: I return the user to the flat list and perform a refresh. I added a callback passing the name of the cake so realistically if desired the scrollToView could be called to take user to this new item at bottom of list. 


- VCS Approach:
    - As a small project, I opted to use a develop branch and raise a PR for each story piece when I felt completed. 
    - Feature branches ordinarily for larger code pieces / functionality but felt added too much admin overhead. Visibility is retained in the commit log for the merges etc. 
    - Future bugfixes will likely take a similar approach rather than branching off develop. 


# Sidenote

- At my current address my connection is 0.1mb/s, so I had to design for my own bandwidth poverty. However I feel it should be performant despite opting for loading all items in a list v pagination. Felt this was scope creep to add this. More backend functionality.


todo: Add screenshots of app