import React from "react";
import ReactDOM from "react-dom";
import Header from './components/Layout/header';
import Footer from './components/Layout/footer';
import { CakesContainer } from './components/cakesContainer';

const Index = () => {
  return <div><Header />
    <CakesContainer />
    <Footer />
    </div>;
};

ReactDOM.render(<Index />, document.getElementById("index"));
