import React from 'react';
import ReactDOM from 'react-dom';
import { Content, Media, Image, Box, Heading} from 'react-bulma-components/full';
import { CakeModal } from './cakeModal';

export class CakeCard extends React.Component {
    constructor() {
        super()
        this.state = { clickedCake:{} }
      }

    openCakeModal(e) {
        e.preventDefault();

        const cakeID = e.target.id;
        fetch("http://ec2-34-243-153-154.eu-west-1.compute.amazonaws.com:5000/api/cakes/" + cakeID)
        .then( response => {
            if (!response.ok) { throw response }
            return response.json()
        })
        .then( data => {
            this.setState({clickedCake:data})
        })
        .catch( err => {})
    }  

    render() {
        if(this.props.cakeArr < 1) {
            return (<Heading style={{textAlign:'center'}}>Hmmmm.... No cakes available.</Heading>)      
        } else {
            return (
            <div>
              <CakeModal cake={this.state.clickedCake} />
                {this.props.cakeArr.map(cake => (
                    <Box key={cake.id}>
                        <Media>
                        <Media.Item>
                            <Image size="1by1" alt="64x64" src={cake.imageUrl} />
                        </Media.Item>
                        <Media.Item>
                            <Content>
                            <Heading size={4} style={{padding:'2rem'}}><a id={cake.id} onClick={(e) => { this.openCakeModal(e)}}>{cake.name}</a></Heading>
                            </Content>
                        </Media.Item>
                        </Media>
                    </Box>
                ))}
            </div>
            )
        }



    }
}


// {
//     id: <number>, 
//     name: <string>, 
//     comment: <string>, 
//     imageUrl: <string>, 
//     yumFactor: <number> 
//     }
    