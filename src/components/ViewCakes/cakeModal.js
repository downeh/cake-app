import React from 'react';
import ReactDOM from 'react-dom';
import style from './css/cakeModal.css';
import { Content, Media, Image, Box, Heading, Modal, Button, Section } from 'react-bulma-components/full';

export class CakeModal extends React.Component {
    constructor() {
        super()
        this.state = { show:false, cake:{} }
      }

      componentDidUpdate(prevProps) {
        if (this.props.cake.name && this.props.cake !== prevProps.cake) {
          this.setState({cake:this.props.cake});
          this.open();
        }
      }
      
      open() {
          this.setState({ show: true }) 
    }
      close() {
          this.setState({ show: false })
    }

    render() {
            return (
                <Modal show={this.state.show} onClose={() => { this.close() }}>
                <Modal.Content>
                <Media>
                    <Media.Item>
                        <Image size="3by2" alt={this.state.cake.name} src={this.state.cake.imageUrl} />
                    </Media.Item>
                </Media>
                <Section style={{ backgroundColor: 'white' }}>
                    <div className={style.cakeYumRating}>{this.state.cake.yumFactor}</div>
                        <Heading>{this.state.cake.name}</Heading>
                        <Heading size={4}>Comment</Heading>
                        <Box>{this.state.cake.comment}</Box>
                </Section>
                </Modal.Content>
                </Modal>
            )      
    }
}