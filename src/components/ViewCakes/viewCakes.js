import React from 'react';
import ReactDOM from 'react-dom';
import {CakeCard} from './cakeCard';
import { Columns, Button, Heading, Section } from 'react-bulma-components/full';

export class ViewCakes extends React.Component {
    constructor() {
        super()
        this.state = { loading:true, loadingMessage:'Loading Those tasty cakes...', cakeArr: [] }
      }

      componentWillMount() {
        this.fetchCakes();
    }

    componentDidUpdate(prevProps) {
      if (this.props.newCakeAdded !== prevProps.newCakeAdded) {
        //Added should I want to scroll user to the new cake added in the list
        this.fetchCakes();
      }
    }

    fetchCakes() {
      this.setState({loading:true, cakeArr:[]})
        fetch("http://ec2-34-243-153-154.eu-west-1.compute.amazonaws.com:5000/api/cakes")
          .then( response => {
            if (!response.ok) { throw response }
            return response.json()
          })
          .then( data => {
            this.setState({cakeArr:data, loading:false})
          })
          .catch( err => {
            this.setState({loading:true, loadingMessage:'Is the host down?'})
          })
    }



    render() {
      if(this.state.loading) {
        return (<Section><Heading style={{textAlign:'center'}}>{this.state.loadingMessage}</Heading></Section>)
      } else
        return (
            <Columns style={{marginBottom:'5rem'}}>
                <Columns.Column size={12}>
                  <Button disabled={this.state.loading} onClick={() => { this.fetchCakes() }}>Refresh Cakes</Button>
                </Columns.Column>
              <Columns.Column>
             <CakeCard cakeArr={this.state.cakeArr}/></Columns.Column>
            </Columns>
        )
    }
}