import React from 'react';
import ReactDOM from 'react-dom';
import style from './css/addCakes.css';
import { Columns, Button, Box, Section, Dropdown } from 'react-bulma-components/full';
var validUrl = require('valid-url');

export class AddCakes extends React.Component {
    constructor() {
        super()
        this.state = { toggleAddView: false, selected:"5" }
      }

    toggleAddCakeView() {
      if(this.state.toggleAddView) {
        this.setState({toggleAddView:false})
      } else {
        this.setState({toggleAddView:true, validation:''})
      }
    }

    onChange(selected) {
      this.setState({ selected });
    };

    onCakeSubmission(e) {
      e.preventDefault();
      const cakeName = this.cakeNameInput.value;
      const cakeComment = this.cakeNameComment.value;
      const cakeURL = this.cakeNameURL.value;
      const cakeYumRating = this.state.selected;

      if(cakeName && cakeComment && cakeURL && cakeYumRating && validUrl.isUri(cakeURL)) {
        this.setState({validation:"Submitting..."})
        this.submitCake(cakeName, cakeComment, cakeURL, cakeYumRating)
      } else {
        this.setState({validation:"Please ensure all fields completed and image URL is a valid http / https url"})
      }
    }

    submitCake(cakeNameValue, cakeCommentValue, cakeURLValue, cakeYumRatingValue) {
        fetch('http://ec2-34-243-153-154.eu-west-1.compute.amazonaws.com:5000/api/cakes', {
          method: 'post',
          body: JSON.stringify({
            name: cakeNameValue,
            comment: cakeCommentValue,
            imageUrl: cakeURLValue,
            yumFactor: cakeYumRatingValue
          }),
          headers: {
            'Content-Type': 'application/json',
          }
        })
        .then( response => {
          if (!response.ok) { throw response }
          this.setState({validation:'Thanks for submitting ' + cakeNameValue + '. Returning to list...'})
          setTimeout(() => {
            this.toggleAddCakeView();
            this.props.callbackUpdate(cakeNameValue);
          }, 2000);
        })
        .catch( err => {
          this.setState({validation:'Failed to Post. View console for more details.'})
        })
      }

    render() {
      if(this.state.toggleAddView) {
        return (
          <Section>
            <Columns>
              <Columns.Column size={12}>
                <Box>
                  <form className={style.addCakesForm}>
                    <input placeholder="Cake Name" ref={(input) => this.cakeNameInput = input}></input>
                    <input placeholder="Comments?" ref={(input) => this.cakeNameComment = input}></input>
                    <input placeholder="URL" ref={(input) => this.cakeNameURL = input}></input>
                    <Columns style={{margin:'2rem'}}>
                      <Columns.Column size={8}>
                        <p>How would you rate this cake?</p>
                        <Dropdown value={this.state.selected} onChange={(selected) => { this.onChange(selected) }}>
                            <Dropdown.Item value="1">1 - I would avoid</Dropdown.Item>
                            <Dropdown.Item value="2">2 - Not THE worst</Dropdown.Item>
                            <Dropdown.Item value="3">3 - Alright</Dropdown.Item>
                            <Dropdown.Item value="4">4 - Nice </Dropdown.Item>
                            <Dropdown.Item value="5">5 - The best</Dropdown.Item>
                          </Dropdown>
                      </Columns.Column> 
                      <Columns.Column size={4}>
                          <Button size="medium" color={'primary'} onClick={(e) => { this.onCakeSubmission(e)}}>Publish</Button>
                          <Button size="medium" color={'danger'} onClick={() => {this.toggleAddCakeView()}}>Cancel</Button>
                      </Columns.Column>
                    </Columns>
                  </form>
                </Box>
              </Columns.Column>
              <Columns.Column>
               <p align="center">{this.state.validation}</p>
             </Columns.Column>
            </Columns>
          </Section>
        )
      } else {
        return (
          <Columns>
            <Columns.Column offset={10}>
              <Button size="large" color={'primary'} onClick={() => {this.toggleAddCakeView()}}>Add a Cake</Button>
            </Columns.Column>
          </Columns>
        )}
    }
}