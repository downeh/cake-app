import React from "react";
import ReactDOM from "react-dom";

class Header extends React.Component {
    render() {
      return (
        <nav className="navbar" aria-label="main navigation">
            <div className="navbar-brand">
            <a className="navbar-item">Cake App</a>
            </div>
        </nav>
      )
    }
  }

  export default Header