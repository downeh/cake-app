import React from 'react'

const Footer = () => (
  <footer className="footer" style={{position:'fixed', bottom:'0px', width:'100%', padding:'1rem'}}>
    <div className="container">
      <div className="content has-text-centered">
        <p>Cakes app for the purposes of demonstration by <strong>GrantDownie</strong></p>
      </div>
    </div>
  </footer>
)

export default Footer