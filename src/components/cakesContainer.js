import React from 'react';
import ReactDOM from 'react-dom';
import { Columns, Container } from 'react-bulma-components/full';
import { ViewCakes } from './ViewCakes/viewCakes';
import { AddCakes } from './AddCakes/addCakes';

export class CakesContainer extends React.Component {
    constructor() {
        super()
        this.updateReceived = this.updateReceived.bind(this);
        this.state = { addedCake:'' }
      }

    updateReceived(Name) {
      this.setState({addedCake:Name});
    }

    render() {
        return (
            <Columns> 
              <Container>
                <Columns.Column>
                  <AddCakes callbackUpdate={this.updateReceived} />
                </Columns.Column>
                <Columns.Column>
                  <ViewCakes newCakeAdded={this.state.addedCake} />
                </Columns.Column>
              </Container>
          </Columns>
        )
    }
}